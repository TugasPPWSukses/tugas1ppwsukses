from django.db import models

# Create your models here.
class News(models.Model):
    judul = models.CharField(max_length = 100)
    gambar = models.CharField(max_length=200)
    penulis = models.CharField(max_length=200)
    readmore = models.CharField(max_length=500, default ="#")
