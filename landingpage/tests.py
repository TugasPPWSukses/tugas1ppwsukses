
from django.test import TestCase, Client
from .models import News
from program.models import Program
from django.urls import resolve
from .views import *
# Create your tests here.
class LandingPageUnitTest(TestCase):
    def test_landingpage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_landingpage_has_news_models(self):
        News.objects.create(judul="test", penulis="abscdlre", gambar="https://akcdn.detik.net.id/community/media/visual/2018/10/17/d464f46c-4711-4a1f-9a0b-14035b77d51d_169.jpeg?w=780&q=90", readmore="https://news.detik.com/berita/d-4260390/menkum-sebagian-besar-napi-tahanan-di-palu-sudah-kembali?_ga=2.53561896.1645967930.1539762383-1677352268.1526769961")
        count_all_stats = News.objects.all().count()
        self.assertEqual(count_all_stats, 1)
    def test_landingpage_using_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landingPage)
    def test_landingpage_show_news(self):
        News.objects.create(judul="test", penulis="abscdlre", gambar="https://akcdn.detik.net.id/community/media/visual/2018/10/17/d464f46c-4711-4a1f-9a0b-14035b77d51d_169.jpeg?w=780&q=90", readmore="https://news.detik.com/berita/d-4260390/menkum-sebagian-besar-napi-tahanan-di-palu-sudah-kembali?_ga=2.53561896.1645967930.1539762383-1677352268.1526769961")
        response = Client().get('/')
        news_content = "Berita"
        html_response = response.content.decode('utf8')
        self.assertIn(news_content,html_response)
 
