from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import *
from program.models import Program

# Create your views here.
def landingPage(request):
	news = News.objects.all()
	donationPrograms = Program.objects.all()
	return render(request, 'landing_page.html', {'donationPrograms': donationPrograms, 'news': news})

# Create your views here.

