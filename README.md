# Repository Tugas 1 & 2 PPW-F (Kelompok 12)

## Anggota kelompok
- Fivi Melinda (1706984594)
- M. Dhiyaulhaq Nugraha (1706043525)
- Stevany Supardi (1706043916)
- Shafira Dyah Pradita (1706984726)

## Link Website Herokuapp
[https://tugas1-ppw-sukses.herokuapp.com/](https://tugas1-ppw-sukses.herokuapp.com/)


## Status Website 
[![pipeline status](https://gitlab.com/TugasPPWSukses/tugas1ppwsukses/badges/master/pipeline.svg)](https://gitlab.com/TugasPPWSukses/tugas1ppwsukses/commits/master)
[![coverage report](https://gitlab.com/TugasPPWSukses/tugas1ppwsukses/badges/master/coverage.svg)](https://gitlab.com/TugasPPWSukses/tugas1ppwsukses/commits/master)