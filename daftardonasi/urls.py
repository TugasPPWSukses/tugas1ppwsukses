from django.conf.urls import url
from django.urls import path

from .views import listDonasi

app_name = 'daftardonasi'

urlpatterns = [
    path('', listDonasi, name='list_donasi')
]