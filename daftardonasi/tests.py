from django.test import TestCase, Client
from django.http import HttpRequest
from .views import listDonasi
from .urls import *

# Create your tests here.
class daftarDonasiTest(TestCase):
    def test_daftar_donasi_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_daftar_donasi_url_is_not_exist(self):
        response = Client().get('/doesnt_exist')
        self.assertEqual(response.status_code, 404)