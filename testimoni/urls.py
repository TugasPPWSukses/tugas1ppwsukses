from django.urls import path
from .views import testimoni, tentangKami, saveTestimoni

urlpatterns = [
    path('', tentangKami, name="tentangKami"),
    path('testimoni/', testimoni, name="testimoni"),
    path('save/', saveTestimoni, name="saveTestimoni")
]
