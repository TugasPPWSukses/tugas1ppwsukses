from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import tentangKami
from .models import Testimoni


# Create your tests here.

class TentangKamiPageUnitTest(TestCase):
    def test_about_page_url_is_exists(self):
        response = Client().get('/tentangKami/')
        self.assertEqual(response.status_code, 200)

    def test_about_page_using_about_function(self):
        found = resolve('/tentangKami/')
        self.assertEqual(found.func, tentangKami)

    def test_about_page_using_templates(self):
        response = Client().get('/tentangKami/')
        self.assertTemplateUsed(response, 'tentangKami.html')

    def test_testimony_model_can_add_new(self):
        Testimoni.objects.create(name='Test', text='test123')
        testimony_counts_all_entries = Testimoni.objects.all().count()
        self.assertEqual(testimony_counts_all_entries, 1)

    def test_self_func_name(self):
        Testimoni.objects.create(name='Test', text='test123')
        testi = Testimoni.objects.get(name='Test')
        self.assertEqual(str(testi), testi.name)
2