from django.http import JsonResponse
from django.shortcuts import render
from .models import Testimoni

# Create your views here.
content = {}


def tentangKami(request):
    content['is_login'] = True
    content['data'] = list(Testimoni.objects.all())
    return render(request, 'tentangKami.html', content)


def testimoni(request):
    if request.method == 'POST':
        text = request.POST['text']
        name = request.session['name']
        Testimoni.objects.create(name=name, text=text)
        return JsonResponse({'saved': True})
    return JsonResponse({'saved': False})

def saveTestimoni(request):
    if request.method == 'POST':
        text = request.POST['text']
        name = request.user.first_name + " " + request.user.last_name
        Testimoni.objects.create(name=name, text=text)
        return JsonResponse({'saved': True, 'name': name, 'text': text})
    return JsonResponse({'saved': False})



