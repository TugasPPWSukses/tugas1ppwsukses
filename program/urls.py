from django.urls import path
from .views import program_view

urlpatterns = [
    path('<str:judul>', program_view)
]
