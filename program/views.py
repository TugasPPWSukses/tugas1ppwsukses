from django.shortcuts import render
from .models import Program
from django.shortcuts import get_object_or_404
from form_donasi.models import formDonasiModels
# Create your views here.


def program_view(request, judul):
    program = get_object_or_404(Program, pk=judul)
    percent = program.donasiAwal/program.targetDonasi*100
    sisa = 100 - percent
    listDonatur = formDonasiModels.objects.all().filter(nama_program=judul)
    return render(request, 'program.html', {'judul': program.judul, 'target': program.targetDonasi,
                                                 'sekarang':program.donasiAwal, 'isi':program.isi,
                                                 'foto': program.gambar,
                                                 'percent': "{0:.2f}".format(percent),
                                                 'sisa': "{0:.2f}".format(sisa),
                                                 'listDonatur': listDonatur, 'donatur': listDonatur.count()})