from django.db import models

# Create your models here.


class Program(models.Model):
    judul = models.CharField(max_length=100, primary_key=True)
    targetDonasi = models.DecimalField(max_digits=20, decimal_places=0, default=0)
    donasiAwal = models.DecimalField(max_digits=20, decimal_places=0, default=0)
    isi = models.TextField(default='testinggggggg')
    gambar = models.CharField(max_length=350, default='#')
