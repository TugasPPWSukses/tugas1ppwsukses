from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import program_view
from .models import Program

# Create your tests here.


class ProgramUnitTest(TestCase):
    def test_model_can_create_new_program(self):
        Program.objects.create(judul='test', targetDonasi=2000, donasiAwal=0, isi="nyobaindoanghehe", gambar='hehe.jpg')
        count = Program.objects.all().count()
        self.assertEqual(count, 1)

    def test_program_url_is_exist(self):
        Program.objects.create(judul='test', targetDonasi=2000, donasiAwal=0, isi="nyobaindoanghehe", gambar='hehe.jpg')
        response = Client().get('/program/test')
        self.assertEqual(response.status_code, 200)

    def test_program_using_ProgramView_func(self):
        found = resolve('/program/test')
        self.assertEqual(found.func, program_view)

    def test_program_render_result(self):
        judul = 'hadededeh'
        targetDonasi='2000'
        donasiAwal='0'
        isi = "nyobaindoanghehe"
        Program.objects.create(judul='hadededeh', targetDonasi=2000, donasiAwal=0, isi="nyobaindoanghehe", gambar='hehe.jpg')
        response = Client().get('/program/hadededeh')
        html_response = response.content.decode('utf8')
        self.assertIn(judul, html_response)
        self.assertIn(targetDonasi, html_response)
        self.assertIn(donasiAwal, html_response)
        self.assertIn(isi, html_response)

    def test_program_using_template(self):
        Program.objects.create(judul='hadededeh', targetDonasi=2000, donasiAwal=0, isi="nyobaindoanghehe", gambar='hehe.jpg')
        response = Client().get('/program/hadededeh')
        self.assertTemplateUsed(response, 'program.html')

# Create your tests here.
