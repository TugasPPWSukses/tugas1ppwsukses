from django.http import HttpResponseRedirect
from django.shortcuts import render

from .forms import submitDonasiForm
from .models import formDonasiModels
from register.models import InputDonatur

def index(request):
    isiForms = formDonasiModels.objects.all()
    response = {'forms': submitDonasiForm, 'result': isiForms}
    return render(request, 'form_donasi.html', response)

def form_donasi_post(request):
    response = {}
    if request.method == "POST":
        cekAnonim = request.POST.getlist('cekAnon')
        if(cekAnonim):
            response['nama_lengkap'] = request.POST['nama_lengkap']
        else :
            response['nama_lengkap'] = 'Anonymous'
        response['nama_program'] = request.POST['nama_program']
        response['email'] = request.POST['email']
        response['jumlah_uang'] = request.POST['jumlah_uang']
        response['dukungan'] = request.POST['dukungan']
        forms = formDonasiModels(nama_program=response['nama_program'], nama_lengkap=response['nama_lengkap'], email=response['email'], jumlah_uang=response['jumlah_uang'], dukungan=response['dukungan'])
        if request.user.is_authenticated:
            if request.user.email == request.POST['email']:
                forms.save()
                return HttpResponseRedirect('/listDonatur')
        return HttpResponseRedirect('/form_donasi')
    else :
        return HttpResponseRedirect('/form_donasi')
