from django.http import HttpRequest
from django.test import TestCase, Client
from .views import *
from .urls import *
from .models import formDonasiModels
from .forms import submitDonasiForm

# Create your tests here.
class tugas1UnitTest(TestCase):
    def test_story_6_url_is_exist(self):
        response = Client().get('/form_donasi/')
        self.assertEqual(response.status_code, 200)

    def test_tugas_1_using_form_donasi_template(self):
        response = Client().get('/form_donasi/')
        self.assertTemplateUsed(response, 'form_donasi.html')

    def test_tugas_1_returns_correct_html(self):
        request = HttpRequest()
        response = index(request)
        html = response.content.decode('utf8')
        self.assertIn('Tampilkan nama donatur', html)

    def test_tugas_1_url_is_not_exist(self):
        response = Client().get('/not_exist')
        self.assertEqual(response.status_code, 404)

    def test_model_can_create_all(self):
        # Creating a new activity
        object1 = formDonasiModels.objects.create(nama_lengkap="Nama Saya", email="a@gmail.com", jumlah_uang=100000, dukungan="Semangat !")

        # Retrieving all available activity
        counting_all_status = formDonasiModels.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_form_post_and_render_the_result(self):
        nama_program = 'Bersama, Bantu Palu, Donggala'
        nama = 'Anonymous'
        email = "test@sinarperak.com"
        jumlah_uang = 100000
        dukungan = "Semangat !"
        response_post = Client().post('/form_donasi/form_donasi_post',
                                      {'nama_program' : nama_program,'nama': nama, 'email': email, 'jumlah_uang': jumlah_uang, 'dukungan': dukungan, 'cekAnon':{}})
        self.assertEqual(response_post.status_code, 302)

