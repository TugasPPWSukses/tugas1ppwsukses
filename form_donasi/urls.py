from django.conf.urls import url
from django.urls import path

from .views import form_donasi_post, index

app_name = 'form_donasi'

urlpatterns = [
    path('', index, name='index'),
    path('form_donasi_post', form_donasi_post, name='form_donasi_post')
]