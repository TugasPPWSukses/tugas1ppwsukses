from django.db import models
from .forms import *

# Create your models here.
class formDonasiModels(models.Model):
    nama_program = models.CharField(max_length = 1000000, choices = PROGRAM_CHOICES)
    nama_lengkap = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    jumlah_uang = models.BigIntegerField()
    dukungan = models.CharField(max_length=30)
    created_date = models.DateTimeField(auto_now_add=True)