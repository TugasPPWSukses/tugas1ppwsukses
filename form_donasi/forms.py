from django import forms

PROGRAM_CHOICES = (
    ("Bersama, Bantu Palu, Donggala", ("Bersama, Bantu Palu, Donggala")),
    ("Kami Terancam Hanyut, Butuh Jembatan", ("Kami Terancam Hanyut, Butuh Jembatan")),
)

class submitDonasiForm(forms.Form):
    nama_program = forms.ChoiceField(choices=PROGRAM_CHOICES, label="Program")#, initial='', widget=forms.Select(), required=True)
    nama_lengkap = forms.CharField(required=True, label='', widget=forms.TextInput(attrs={'class': 'form-control',
                                                                 'placeholder': "Nama Lengkap"}))
    email = forms.EmailField(required=True, label='', widget=forms.TextInput(attrs={'type': 'email',
                                                                     'placeholder': 'Alamat E-Mail'}))
    jumlah_uang = forms.IntegerField(required=True, label='', widget=forms.TextInput(attrs={'type': 'number',
                                                                     'placeholder': 'Jumlah uang yang ingin didonasikan',
                                                                          'min':'10000'}))
    dukungan = forms.CharField(required=True, label='',
                               widget=forms.TextInput(attrs={'class': 'form-control',
                                                             'placeholder': "Dukungan untuk sahabat kita"}))