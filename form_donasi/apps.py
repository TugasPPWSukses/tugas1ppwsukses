from django.apps import AppConfig


class FormDonasiConfig(AppConfig):
    name = 'form_donasi'
