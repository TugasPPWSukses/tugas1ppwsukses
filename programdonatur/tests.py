from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import listDonatur

# Create your tests here.
class UnitTest(TestCase):
        def test_listDonatur_url_is_exist(self):
                response = Client().get('/listDonatur/')
                self.assertEqual(response.status_code, 200)

        def test_listDonatur_using_listDonatur_func(self):
                found = resolve('/listDonatur/')
                self.assertEqual(found.func, listDonatur)

        def test_listDonatur_title_is_exist(self):
                request = HttpRequest()
                response = listDonatur(request)
                string = response.content.decode("utf8")
                self.assertIn("#Terima kasih</span> telah berdonasi bersama kami", string)
#def test_listDonatur_can_create_new_box(self):
#cek kalo ada yg donasi masuk list ga

	
