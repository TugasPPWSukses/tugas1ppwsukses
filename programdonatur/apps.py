from django.apps import AppConfig


class ProgramdonaturConfig(AppConfig):
    name = 'programdonatur'
