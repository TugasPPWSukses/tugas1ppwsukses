from django.urls import path
from .views import listDonatur


urlpatterns = [
    path('', listDonatur, name='listDonatur')
]