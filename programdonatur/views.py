from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

from form_donasi.models import formDonasiModels
# Create your views here.

response = {}
def listDonatur(request):
    donatur = formDonasiModels.objects.all()
    response['donatur'] = donatur
    return render(request, 'listdonatur.html' , response)