from django import forms
from django.core.exceptions import ValidationError
from .models import InputDonatur
from django.utils.translation import ugettext_lazy as _


class FormDonatur(forms.Form):
    nama = forms.CharField(required=True, max_length=10000, widget=forms.TextInput(attrs={'placeholder': 'Nama Lengkap'}))
    date = forms.DateField(required=True, widget=forms.TextInput(attrs={'type':'date', 'placeholder': 'Tanggal Lahir'}))
    email = forms.CharField(required=True, max_length=10000,
                            widget=forms.DateInput(attrs={'type': 'email', 'placeholder': 'Email'}))
    password = forms.CharField(required=True, max_length=10000,
                               widget=forms.TextInput(attrs={'type': 'password', 'placeholder': 'Password'}))

    def clean(self):
        super(FormDonatur, self).clean()

        cleaned_data = self.cleaned_data

        email = cleaned_data.get("email")

        if InputDonatur.objects.filter(email=email).exists():
            unique_email_constraints_error = ValidationError(_("Your e-mail has been registered"))
            self.add_error("email", unique_email_constraints_error)
            print("Error")
            raise unique_email_constraints_error

        return cleaned_data

