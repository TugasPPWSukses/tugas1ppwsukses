from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class InputDonatur(AbstractUser):
    nama = models.CharField(max_length=10000, null=True, blank=True)
    date = models.DateField(null=True, blank=True)
    email = models.CharField(max_length=10000, unique=True, error_messages={'unique': 'Your email has been registered'}, null=True, blank=True)
    password = models.CharField(max_length=10000, null=True, blank=True)

    def __str__(self):
        return self.nama