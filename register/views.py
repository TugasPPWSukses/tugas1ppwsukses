from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import FormDonatur
from .models import InputDonatur
from django.contrib.auth import logout, login
from landingpage.views import landingPage


# Create your views here.

def register(request):
    form = FormDonatur(request.POST)
    if request.method == 'POST' and form.is_valid():
        data = form.cleaned_data
        InputDonatur.objects.create(**data)
        return HttpResponseRedirect('/')
    content = {'form': form}
    return render(request, 'register.html', content)

def log_out(request):
    request.session.flush()
    logout(request)
    return redirect('/')
    