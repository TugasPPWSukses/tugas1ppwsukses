from django.urls import path
from .views import register as register

urlpatterns = [
    path('', register, name="register")
]
