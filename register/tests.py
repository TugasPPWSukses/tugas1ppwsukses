from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import register, log_out
from .models import InputDonatur
from .forms import FormDonatur


# Create your tests here.
class RegisterUnitTest(TestCase):

    def test_url_page_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_story_using_index_function(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_model_can_add_new_status(self):
        InputDonatur.objects.create(nama='han', date="2018-10-12", email="a@heroku.com", password="123")
        counting_all_status_message = InputDonatur.objects.all().count()
        self.assertEqual(counting_all_status_message, 1)

    def test_if_form_is_blank(self):
        form = FormDonatur(data={'nama': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['nama'][0],
            'This field is required.',
        )

    def test_form_post_and_render_the_result(self):
        test = 'Anonymous'
        date = '1999-10-12'
        email = "hany@sinarperak.com"
        password = '123'
        response_post = Client().post('/register/', {'nama': test, 'date': date, 'email': email, 'password': password})
        self.assertEqual(response_post.status_code, 302)

    def test_self_func(self):
        InputDonatur.objects.create(nama='HELLO', date="2018-10-12", email="a@heroku.com", password="123")
        status = InputDonatur.objects.get(nama='HELLO')
        self.assertEqual(str(status), status.nama)

    def test_text_max_name_length(self):
        nama = InputDonatur.objects.create(nama="aaa", date="2018-10-12", email="a@heroku.com", password="123")
        self.assertLessEqual(len(str(nama)), 30)

    # Login fitur test
    def test_using_log_out_func(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, log_out)